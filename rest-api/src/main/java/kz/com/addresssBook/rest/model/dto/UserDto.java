package kz.com.addresssBook.rest.model.dto;

import lombok.Data;

@Data
public class UserDto {

    private Long id;
    private String firstname;
    private String lastname;
    private String phone;
}
