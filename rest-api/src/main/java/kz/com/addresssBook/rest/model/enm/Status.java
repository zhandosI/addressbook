package kz.com.addresssBook.rest.model.enm;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
