package kz.com.addresssBook.rest.controller;

import kz.com.addresssBook.rest.model.dto.ResponseDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

public class BaseController {

    private static final String SUCCESS = "success";

    protected ResponseSuccessBuilder success() {
        return new ResponseSuccessBuilder(SUCCESS);
    }

    protected static class ResponseSuccessBuilder {

        private static final List EMPTY_LIST_BODY = Collections.emptyList();

        private String resStatus;
        private HttpStatus httpStatus;
        private HttpHeaders headers;
        private Object body;

        private ResponseSuccessBuilder(String resStatus) {
            this.resStatus = resStatus;
        }

        public ResponseSuccessBuilder withStatus(HttpStatus status) {
            this.httpStatus = status;
            return this;
        }

        public ResponseSuccessBuilder withHeaders(HttpHeaders headers) {
            this.headers = headers;
            return this;
        }

        public ResponseSuccessBuilder withBody() {
            this.body = EMPTY_LIST_BODY;
            return this;
        }

        public ResponseSuccessBuilder withBody(Object body) {
            this.body = body;
            return this;
        }

        public ResponseEntity<?> build() {
            if (httpStatus == null)
                httpStatus = HttpStatus.OK;

            return ResponseEntity
                    .status(httpStatus)
                    .headers(headers)
                    .body(new ResponseDto(resStatus, body));
        }
    }



}
