package kz.com.addresssBook.rest.model.entity;


import kz.com.addresssBook.rest.model.enm.Status;
import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @PrePersist
    private void prePersist() {
        if (status == null)
            status = Status.ACTIVE;
    }

}
