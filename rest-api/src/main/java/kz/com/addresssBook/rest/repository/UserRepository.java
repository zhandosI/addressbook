package kz.com.addresssBook.rest.repository;

import kz.com.addresssBook.rest.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
