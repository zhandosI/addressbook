package kz.com.addresssBook.rest.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDto {

    @JsonProperty(value = "status")
    private String status;

    @JsonProperty(value = "data")
    private Object data;

    public ResponseDto(String status, Object data) {
        this.status = status;
        this.data = data;
    }

}
