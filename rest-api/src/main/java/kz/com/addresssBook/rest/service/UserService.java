package kz.com.addresssBook.rest.service;

import kz.com.addresssBook.rest.model.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();
    void save(User user);
    void delete(Long id);
    User update(Long userId, User user);
    User getById(Long id);
}
