package kz.com.addresssBook.rest.controller;

import kz.com.addresssBook.rest.model.dto.UserDto;
import kz.com.addresssBook.rest.model.entity.User;
import kz.com.addresssBook.rest.service.UserService;
import kz.com.addresssBook.rest.util.mapper.impl.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController extends BaseController {

    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserRestController(UserService userService,
                              UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllUsers() {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(userMapper.toDto(userService.findAll()))
                .build();
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> createUser(@RequestBody UserDto user) {
        userService.save(userMapper.toEntity(user));
        return success()
                .withStatus(HttpStatus.CREATED)
                .build();
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody UserDto user) {
        User toUpdate = userMapper.toEntity(user);
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(userMapper.toDto(userService.update(id, toUpdate)))
                .build();
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
        return success()
                .withStatus(HttpStatus.OK)
                .withBody()
                .build();
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(userMapper.toDto(userService.getById(id)))
                .build();
    }

}
