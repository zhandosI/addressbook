package kz.com.addresssBook.rest.service.impl;

import kz.com.addresssBook.rest.model.enm.Status;
import kz.com.addresssBook.rest.model.entity.User;
import kz.com.addresssBook.rest.repository.UserRepository;
import kz.com.addresssBook.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public List<User> findAll() {
        try {
            return userRepository.findAll();
        }  catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void save(User user) {
        try {
            userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            User user = userRepository.getOne(id);
            user.setStatus(Status.DELETED);
            userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public User update(Long userId, User user) {
        try {
            User toUpdate = userRepository.getOne(userId);

            toUpdate.setFirstname(user.getFirstname());
            toUpdate.setLastname(user.getLastname());
            toUpdate.setPhone(user.getPhone());

            return userRepository.save(toUpdate);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public User getById(Long id) {
        try {
            return userRepository.findOne(id);
        } catch (Exception e){
            throw e;
        }
    }

}
