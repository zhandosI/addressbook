package kz.com.addresssBook.rest.util.mapper;

import kz.com.addresssBook.rest.model.entity.BaseEntity;

import java.util.List;

public interface EntityDtoMapper <E extends BaseEntity, D> {

    D toDto(E entity);

    E toEntity(D dto);

    List<D> toDto(List<E> entities);

    List<E> toEntity(List<D> dtos);
}
