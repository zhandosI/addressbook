package kz.com.addresssBook.rest.util.mapper.impl;

import kz.com.addresssBook.rest.model.dto.UserDto;
import kz.com.addresssBook.rest.model.entity.User;
import kz.com.addresssBook.rest.util.mapper.AbstractEntityDtoMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends AbstractEntityDtoMapper<User, UserDto> {

    public UserMapper() {
        super(User.class, UserDto.class);
    }
}
