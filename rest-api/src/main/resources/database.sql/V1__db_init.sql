create database addressbook_db;
create user addressbook_db_admin with encrypted password 'P@ssw0rd';
grant all privileges on database addressbook_db to addressbook_db_admin;