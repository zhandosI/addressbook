CREATE TABLE public.users
(
    id bigserial PRIMARY KEY NOT NULL,
    firstname varchar(255),
    lastname varchar(255),
    phone varchar(255),
    status varchar(25) DEFAULT 'ACTIVE' NOT NULL
);
CREATE UNIQUE INDEX users_id_uindex ON public.users (id);
CREATE UNIQUE INDEX users_phone_uindex ON public.users (phone);